FROM golang:1.14
LABEL maintainer="maintainers@gitea.io"
RUN go get -u github.com/mgechev/revive
